import java.util.stream.IntStream;

public class Main {

    private static final int N = 1 << 12;
    private static final int BLOCK = 1 << 10;
    private static int[] curArray = new int[N];
    private static int[] prevArray = new int[N];

    public static void main(String... args) {
        System.out.println("Warmup...");
        for (int i = 0; i < 100; ++i) {
            sequential();
            parallel();
        }
        System.out.println("Benchmark...");
        for (int i = 0; i < 5; ++i) {
            System.out.printf("Run %d:  sequential %s  -  parallel %s\n",
                    i,
                    test(Main::sequential),
                    test(Main::parallel));
        }
    }

    private static void sequential() {
        prevArray[0] = 1;

        for (int diag = 1; diag < 2 * N + 1; ++diag) {

            int startX = diag < N ? 0 : diag - N + 1;
            int endX = diag / 2;

            for (int x = startX; x < endX; ++x) {
                int y = diag - x;

                int valLeft = x > 0 ? prevArray[x - 1] : 0;
                int valUp = x < y ? prevArray[x] : 0;

                prevArray[x] = valLeft + valUp;
            }
        }
    }

    private static void swap() {
        int[] tmp = prevArray;
        prevArray = curArray;
        curArray = tmp;
    }

    private static int floorDiv(int a, int b) {
        return a / b;
    }

    private static int ceilDiv(int a, int b) {
        return (a - 1) / b + 1;
    }

    private static void parallel() {
        prevArray[0] = 1;

        for (int diag = 1; diag < 2 * N + 1; ++diag) {

            int startX = diag < N ? 0 : diag - N + 1;
            int endX = diag / 2;

            int startXBlock = floorDiv(startX, BLOCK);
            int endXBlock = ceilDiv(endX, BLOCK);

            int finalDiag = diag;
            IntStream.range(startXBlock, endXBlock).parallel().forEach((xBlock) -> {

                for (int x = Math.max(xBlock * BLOCK, startX);
                     x < Math.min((xBlock + 1) * BLOCK, endX);
                     ++x) {

                    int y = finalDiag - x;

                    int valLeft = x > 0 ? prevArray[x - 1] : 0;
                    int valUp = x < y ? prevArray[x] : 0;

                    curArray[x] = valLeft + valUp;
                }
            });
            swap();
        }
    }

    private static String test(Runnable runnable) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++)
            runnable.run();
        long elapsed = System.currentTimeMillis() - start;
        return String.format("%4.2fs", elapsed / 1000.0);
    }

}
