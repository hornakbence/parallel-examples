#include <iomanip>
#include <chrono>
#include <iostream>

std::chrono::steady_clock::time_point stopper;

void stopperStart() {
    stopper = std::chrono::steady_clock::now();
}

long stopperStop() {
    std::chrono::steady_clock::duration duration = std::chrono::steady_clock::now() - stopper;
    auto durationMs = std::chrono::duration_cast<std::chrono::milliseconds>(duration);
    long ms = durationMs.count();

    return ms;
}

template<class T>
long test(T functor) {
    stopperStart();
    for (int i = 0; i < 10; ++i)
        functor();
    return stopperStop();
}

template<class T1, class T2>
void benchmark(T1 function1, T2 function2,
               int warmupCount, int benchmarkCount, int benchmarkRepeat) {
    std::cout << "Warmup" << std::endl;

    for (int i = 0; i < warmupCount; ++i) {
        function1();
        function2();

    }

    std::cout << "Benchmark:" << std::endl;
    for (int j = 0; j < benchmarkCount; ++j) {
        std::cout << "Test1: " << std::setw(5) << test(function1) << "ms "
                  << "Test2: " << std::setw(5) << test(function2) << "ms" << std::endl;
    }
}

template<class T1, class T2, class T3>
void benchmark(T1 function1, T2 function2, T3 function3,
               int warmupCount, int benchmarkCount, int benchmarkRepeat) {
    std::cout << "Warmup" << std::endl;

    for (int i = 0; i < warmupCount; ++i) {
        function1();
        function2();
        function3();
    }

    std::cout << "Benchmark:" << std::endl;
    for (int j = 0; j < benchmarkCount; ++j) {
        std::cout << "Test1: " << std::setw(5) << test(function1) << "ms "
                  << "Test2: " << std::setw(5) << test(function2) << "ms "
                  << "Test3: " << std::setw(5) << test(function3) << "ms" << std::endl;
    }
}