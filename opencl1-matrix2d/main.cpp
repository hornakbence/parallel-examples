#define __CL_ENABLE_EXCEPTIONS

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else

#include <CL/cl.hpp>

#endif

#include <string>
#include <iostream>
#include <iomanip>

// Source code of the GPU program
const std::string PROGRAM_SOURCE = R"(
__kernel void fillMatrix(__global int *mat, const unsigned n) {
    unsigned i = get_global_id(0); // see globalSize below
    unsigned j = get_global_id(1); // see globalSize below

    mat[n * i + j] = i + j;
}
)";

// Size of the square matrix. Must be a multiple of LOCAL_SIZE
const unsigned SIZE = 64;
// LOCAL_SIZE x LOCAL_SIZE sized blocks will be calculated simultaneously. It cannot be much higher, since hardware
// device must have LOCAL_SIZE x LOCAL_SIZE processing units.
const unsigned LOCAL_SIZE = 16;

// A row major square matrix
int mat[SIZE * SIZE];
// Size of mat in bytes
const size_t mat_size = sizeof(mat); // SIZE * SIZE * sizeof(int)


// Headers
cl::Context createContext();

void runProgram(cl::CommandQueue &queue, cl::Kernel &kernel);

void copyToHostMemory(cl::CommandQueue &queue, cl::Buffer &matBuffer);

void printMat();


int main() {

    try {
        // OpenCL needs a Context object to manage command queues, memory, program and kernel objects
        cl::Context context = createContext();
        // List of available devices in the context
        std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
        // A CommandQueue can process commands such as kernel functions and memory upload/download commands
        cl::CommandQueue queue(context, devices[0]);

        // Represents the program that the GPU is going to run
        cl::Program program(
                PROGRAM_SOURCE,
                true // build it now
        );

        // Kernel is a function in the OpenCL program, which is marked with __kernel qualifier
        cl::Kernel kernel(
                program,
                "fillMatrix" // The exact name of the kernel function in the source code
        );

        // One dimensional array on the device. In this example it is only written by the GPU
        cl::Buffer matBuffer(context, CL_MEM_WRITE_ONLY, mat_size);

        // Arguments for the kernel
        kernel.setArg(0, matBuffer);
        kernel.setArg(1, SIZE);

        // Starts the execution of the kernel and blocks until it is done
        runProgram(queue, kernel);
        // Copies back from the GPU's memory (represented by matBuffer) to host memory (mat)
        copyToHostMemory(queue, matBuffer);

        // Prints mat
        printMat();

        return 0;

    } catch (cl::Error &error) {
        std::cerr << error.what() << " " << error.err() << std::endl;
        return -1;
    }

}


cl::Context createContext() {

    // Query platforms
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.empty())
        throw std::runtime_error("Platform size 0\n");

    // Get list of devices on default platform and create context
    cl_context_properties properties[] =
            {CL_CONTEXT_PLATFORM, (cl_context_properties) (platforms[0])(), 0};
    cl::Context context(CL_DEVICE_TYPE_GPU, properties);

    return context;
}

void runProgram(cl::CommandQueue &queue, cl::Kernel &kernel) {

    // NDRange is 1D, 2D or 3D generalization of size_t.
    // globalSize is SIZE x SIZE, because (i,j) should iterate over all combinations in 0..(SIZE-1) x 0..(SIZE-1) in
    // kernel function
    cl::NDRange globalSize(SIZE, SIZE);
    // localSize is LOCAL_SIZE x LOCAL_SIZE, meaning blocks with this sizes will be computed in parallel
    cl::NDRange localSize(LOCAL_SIZE, LOCAL_SIZE);

    // Event is like a Future in Java. You can .wait() it and block synchronously or you can use .setCallback() to
    // handle events asynchronously.
    cl::Event event;

    // Put a command on the queue. Kernel function will be invoked SIZE x SIZE (globalSize) times.
    queue.enqueueNDRangeKernel(
            kernel,
            cl::NullRange, // No offset, start with (0,0)
            globalSize,
            localSize,
            nullptr, // Which Events should execute, before it can be started (none in this example)
            &event
    );
    event.wait();
}

void copyToHostMemory(cl::CommandQueue &queue, cl::Buffer &matBuffer) {
    // Move data from matBuffer to mat
    queue.enqueueReadBuffer(
            matBuffer, // Source buffer
            CL_TRUE, // Block until it is done
            0, // No offset, start from position 0
            mat_size, // Number of bytes to be copied
            mat // Destination pointer
    );
}

void printMat() {
    // Print mat row by row
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            std::cout
                    << std::setw(4) // padding
                    << mat[SIZE * i + j];
        }
        std::cout << std::endl;
    }
}
