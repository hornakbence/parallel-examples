cmake_minimum_required(VERSION 3.10)
project(matrix2d_opencl)

set(CMAKE_CXX_STANDARD 11)

add_executable(matrix2d_opencl main.cpp)
target_link_libraries(matrix2d_opencl OpenCL)