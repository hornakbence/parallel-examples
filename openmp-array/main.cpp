#include <cmath>
#include "../benchmark/benchmark.hpp"

const int N = 1u << 20u;
float array[N];


void serial() {
    for (int i = 0; i < N; ++i) {
        array[i] = sinf(i);
    }
}
void parallel() {
#pragma omp parallel for
    for (int i = 0; i < N; ++i) {
        array[i] = sinf(i);
    }
}


int main() {
    benchmark(serial, parallel, 100, 10, 10);
    return 0;
}