import java.util.stream.IntStream;

public class Main {

    private static final int N = 1 << 19;
    private static final double[] ARRAY = new double[N];

    private static void serial() {
        for (int i = 0; i < N; i++)
            ARRAY[i] = Math.sin(i);
    }

    private static void totalParallel() {
        IntStream.range(0, N).parallel()
                .forEach((i) -> {
                    ARRAY[i] = Math.sin(i);
                });
    }


    private static String test(Runnable runnable) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++)
            runnable.run();
        long elapsed = System.currentTimeMillis() - start;
        return String.format("%4d", elapsed);
    }

    public static void main(String... args) {
        System.out.println("Warmup...");
        for (int i = 0; i < 100; ++i) {
            serial();
            totalParallel();
        }
        System.out.println("Benchmark...");
        for (int i = 0; i < 5; ++i) {
            System.out.printf("Serial: %s Parallel %s\n",
                    test(Main::serial),
                    test(Main::totalParallel));
        }
    }
}
