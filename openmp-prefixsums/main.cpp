#include <iostream>
#include <omp.h>
#include <cstring>
#include "../benchmark/benchmark.hpp"

const int N = 1u << 18u;
int *arr1, *arr2;

void genArray() {
#pragma omp parallel for
    for (int i = 0; i < N; ++i)
        arr1[i] = i * i;
}


template<class T>
void swap(T &a, T &b) {
    T c = a;
    a = b;
    b = c;
}


void serial() {
    for (int i = 1; i < N; ++i) {
        arr1[i] += arr1[i - 1];
    }

    // std::cout << arr1[N - 1] << std::endl;
}

void parallel() {
    int n = N;
    int i;
    int ar[2][N];
    bool toread = 0;
    int size = 0;
    int *t;

    i = n;
    while (i) {
        size++;
        i >>= 1;
    }

    for (int j = 1; j < size; ++j) {
        toread = !toread;
        t = ar[!toread];
#pragma omp parallel for default(none) private(i) shared(n, j, t, ar, toread)
        for (i = 1; i < n; i++) {

            if (i - (1 << j) >= 0)
                t[i] = ar[toread][i] + ar[toread][i - (1 << j)];
            else t[i] = ar[toread][i];
        }

    }

    // std::cout << arr1[N - 1] << std::endl;
}

void parallel3() {

    int *arr, *partial, *temp;
    int num_threads, work, n;
    int i, mynum, last;
    n = N;
    arr = new int[n];

    i = n;
#pragma omp parallel default(none) private(i, mynum, last) shared(arr, partial, temp, num_threads, work, n)
    {
#pragma omp single
        {
            num_threads = omp_get_num_threads();
            if (!(partial = (int *) malloc(sizeof(int) * num_threads))) exit(-1);
            if (!(temp = (int *) malloc(sizeof(int) * num_threads))) exit(-1);
            work = n / num_threads + 1; /*sets length of sub-arrays*/
        }
        mynum = omp_get_thread_num();
        /*calculate prefix-sum for each subarray*/
        for (i = work * mynum + 1; i < work * mynum + work && i < n; i++)
            arr[i] += arr[i - 1];
        partial[mynum] = arr[i - 1];
#pragma omp barrier
        /*calculate prefix sum for the array that was made from last elements of each of the previous sub-arrays*/
        for (i = 1; i < num_threads; i <<= 1) {
            if (mynum >= i)
                temp[mynum] = partial[mynum] + partial[mynum - i];
#pragma omp barrier
#pragma omp single
            memcpy(partial + 1, temp + 1, sizeof(int) * (num_threads - 1));
        }
        /*update original array*/
        for (i = work * mynum; i < (last = work * mynum + work < n ? work * mynum + work : n); i++)
            arr[i] += partial[mynum] - arr[last - 1];
    }
    delete[] arr;
}

int main() {
    arr1 = new int[N];
    arr2 = new int[N];
    genArray();

    benchmark(serial, parallel, parallel3, 100, 10, 100);

    delete[] arr1;
    delete[] arr2;
    return 0;
}